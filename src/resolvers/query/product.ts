import { COLLECTIONS } from './../../config/constants';
import { IResolvers } from 'graphql-tools';

const queryProductResolvers: IResolvers = {
    Query: {
        async products(_, __, { db }) {
            return await db.collection(COLLECTIONS.PRODUCTS).find().toArray().then(
                (result: Array<object>) => result
            ).catch(() => []);
        }
    }
};

export default queryProductResolvers;