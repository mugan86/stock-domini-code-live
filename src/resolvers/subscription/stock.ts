import { IResolvers } from 'graphql-tools';
import { SUBSCRIPTIONS_EVENT } from '../../config/constants';


const subscriptionStockResolvers: IResolvers = {
    Subscription: {
        changeStock: {
            subscribe: (_, __, { pubsub}) => pubsub.asyncIterator(SUBSCRIPTIONS_EVENT.CHANGE_STOCK),
        },
    }
};

export default subscriptionStockResolvers;