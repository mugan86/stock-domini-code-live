import { COLLECTIONS, SUBSCRIPTIONS_EVENT } from './../../config/constants';
import { IResolvers } from 'graphql-tools';

const resolversProductMutation: IResolvers = {
    Mutation: {
        async addProduct(_, { product }, { db, pubsub }) {
            console.log(product);
            // AQUI HACEMOS LA OPERACION DE AÑADIR EL PRODUCTO CON MONGODB
            return await db.collection(COLLECTIONS.PRODUCTS)
                .insertOne(product).then(async () => {
                    const list = await db.collection(COLLECTIONS.PRODUCTS).find().toArray();
                    pubsub.publish(SUBSCRIPTIONS_EVENT.CHANGE_STOCK, { changeStock: list});
                    return true;
                }).catch(() => false);
        },
        async updateProductStock(_, {code, stock}, {db, pubsub}) {
            return await db.collection(COLLECTIONS.PRODUCTS).updateOne(
                { code },
                {
                  $inc: { stock}
                }
            ).then(async () => {
                const list = await db.collection(COLLECTIONS.PRODUCTS).find().toArray();
                pubsub.publish(SUBSCRIPTIONS_EVENT.CHANGE_STOCK, { changeStock: list});
                return true;
            }).catch(() => false);
        }
    }
};

export default resolversProductMutation;


